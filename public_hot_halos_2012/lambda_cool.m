%Tozzi & Norman cooling function for 0.3 solar metallicity
function lam = lambda_cool(TkeV)
lam=1.e-22*( 8.6e-3*TkeV.^-1.7 + 5.8e-2*TkeV.^0.5 + 6.3e-2);
