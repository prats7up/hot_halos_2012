c200=10.75;
c500=10.75;
err=1e10;
rhs = (2./5.)*c200^3/( log(1+c200)-c200/(1+c200) );
while (err>1e-4)
%for i=1:200
	f = c500^3/( log(1+c500)-c500/(1+c500) ) - rhs;
	%fprime = 3*c500^3/(log(1+c500)-c500/(1+c500)) - c500^4/(1+c500)^2/(log(1+c500)-c500/(1+c500))^2; bug? but get same ans
    fprime = 3*c500^2/(log(1+c500)-c500/(1+c500)) - c500^4/(1+c500)^2/(log(1+c500)-c500/(1+c500))^2;
    %err = abs(fprime/f/c500); #also buggy
    err = abs(f/fprime);
    c500 = c500 - f/fprime
end
