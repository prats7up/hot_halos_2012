% Author: Prateek Sharma, 2012 (originally; modified slightly for public release)
% solid line of Fig. 2 in the following paper is based on this code (with a constant entropy core)
% this includes the potential of a central BCG galaxy
%
%  On the structure of hot gas in haloes: implications for the LX-TX relation and missing baryons
%  Sharma, Prateek; McCourt, Michael; Parrish, Ian J.; Quataert, Eliot
%  MNRAS, 427, 1219
%  http://adsabs.harvard.edu/abs/2012MNRAS.427.1219S
%
% Please cite this paper if you make use of this code in any way.

% model broadly based on Voit et al. 2002; outer density slope for gas is -2.25.
% abyb gives the correct conversion between gas and DM such that f_b is correct.
% later added an option for a different redshift (original paper is for z=0)
%

clear;
imax=1000; %number of radial grid points for profiles 
zred=0; boltz=1.38066e-16; mu=0.62; mue=1.18; mui=1/(1/mu-1/mue); mp=1.673e-24; keV2K=1.1604e7; %redshift and Physical constants
dcrit = 9.21e-30*(1+zred)^3; d200 = 200*dcrit;guniv = 6.67e-8;
r = logspace(-3,0,imax); %radius in units of r200 
tTIbytf_th=10;  %value of min(t_{TI}/t_{ff}) for the core
fid1 = fopen( strcat('table_out_2.25_trat_',int2str(tTIbytf_th),'_BCG.dat'),'wt'); %ascii file with halo-integrated quantities
for k=1:11
switch (k)
    %c500 obtained from conc using a different code; abyb is a normalization factor to make sure fb = (gas mass of halo)/(DM mass of halo) = 0.17; each ascii file below contains the radial profile for an individual halo
    case 1
    conc = 4; c500 = 2.6067; M200 = 3.e15*2e33; fb=0.17; abyb = 0.449714; fid = fopen( strcat('nT_vs_r_3e15_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt'); % 3.10^15 solar mass halo
    case 2
    conc = 5; c500 = 3.3052; M200 = 1.e15*2e33; fb=0.17; abyb = 0.41064; fid = fopen( strcat('nT_vs_r_1e15_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 3
    conc = 6; c500 = 4.0066; M200 = 6.e14*2e33; fb=0.17; abyb = 0.380177; fid = fopen( strcat('nT_vs_r_6e14_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 4
    conc = 7; c500 = 4.7102; M200 = 3.e14*2e33; fb=0.17; abyb = 0.355553; fid = fopen( strcat('nT_vs_r_3e14_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 5
    conc = 8; c500 = 5.4154; M200 = 1.e14*2e33; fb=0.17; abyb = 0.335104; fid = fopen( strcat('nT_vs_r_1e14_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 6
    conc = 9; c500 = 6.1219; M200 = 6.e13*2e33; fb=0.17; abyb = 0.317764; fid = fopen( strcat('nT_vs_r_6e13_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 7
    conc = 9.5; c500 = 6.4756; M200 = 3.e13*2e33; fb=0.17; abyb = 0.310027; fid = fopen( strcat('nT_vs_r_3e13_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 8
    conc = 10.5; c500 = 7.1836; M200 = 1.e13*2e33; fb=0.17; abyb = 0.296073; fid = fopen( strcat('nT_vs_r_1e13_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 9
    conc = 11; c500 = 7.5380; M200 = 6.e12*2e33; fb=0.17; abyb = 0.289751; fid = fopen( strcat('nT_vs_r_6e12_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 10
    conc = 11.5; c500 = 7.8925; M200 = 3.e12*2e33; fb=0.17; abyb = 0.283807; fid = fopen( strcat('nT_vs_r_3e12_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    case 11
    conc = 12; c500 = 8.2472; M200 = 1.e12*2e33; fb=0.17; abyb = 0.278205; fid = fopen( strcat('nT_vs_r_1e12_TIth_',int2str(tTIbytf_th),'_out_2.25_BCG.dat'), 'wt');
    otherwise
end
r200 = ( M200/(200*dcrit*(4*pi/3)) )^(1/3);T200 = guniv*M200*mu*mp/(2*r200);
r500 = r200*c500/conc; M500 = M200*( log(1.+c500) - c500/(1.+c500) );
%NFW profile
rho_dm = ( conc^3/(3*(log(1+conc) - conc/(1+conc))) )./(conc*r.* (1 + conc*r).^2 );
menc_dm = (log(1+ conc*r) - conc*r./(1+conc*r) )/( log (1+conc) - conc/(1+conc) );
%BCG gravity from Mathews et al. 2006
rkpc = r*r200/3.086e21;
g_ast_cgs = ( (rkpc.^0.5975/3.206e-7).^0.9 + (rkpc.^1.849/1.861e-6).^0.9 ).^(-1./0.9);
g_ast_nondim = g_ast_cgs/(guniv*M200/r200^2);
g_acc = menc_dm./r.^2 + g_ast_nondim;
%gas profile not identical to DM, unlike Voit
rho_g = abyb*( conc^3/(3*(log(1+conc) - conc/(1+conc))) )./(conc*r.* (1 + conc*r).^1.25 );
%outer pressure set such that asymptotic entropy profile is close to r^1.1
%p(imax) = 0.09;
p(imax)=0.32;
%p(imax)=0.25;
%obtain unmodified profile assuming a given gas density profile
for i = imax-1:-1:1
    p(i) = p(i+1) + 2*(r(i+1)-r(i))*0.5*(g_acc(i)+g_acc(i+1))*0.5*(rho_g(i)+rho_g(i+1));
end
K1=p./rho_g.^(5/3);
hold on;
loglog(r,K1,'k');
%now convert to dimensional variables to get tTI/tff
tff = sqrt(2*r200^3/guniv/M200)*sqrt(r.^3./menc_dm);
TkeV = T200*(p./rho_g)/(boltz*keV2K); delT = TkeV/100;
tcool = 1.5*(mue*mui/mu)*(mp./(rho_g*fb*d200)).*(T200*p./rho_g)./lambda_cool(TkeV);
dlnLdlnT = log( lambda_cool(TkeV+delT)./lambda_cool(TkeV) )./log( (TkeV+delT)./TkeV ); alpha = (2 - dlnLdlnT);
tTI = (5/3)*tcool./alpha;
%dimensionless temperature; guess density
temp = p./rho_g; dest = rho_g;
cnt=-1;
%determine where tTI/tff<10
for i = imax:-1:1
    if ( tTI(i)<tTIbytf_th*tff(i) )
        cnt=i;
        break;
    end
end
K0=K1(1); %for cases when the core entropy is high already
if (cnt ~= -1)
    K0 = K1(cnt);
end
% if tTI/tff>10 at the outer boundary itself; solve for the pout=p(imax)
% and tTI/tff=10 at the outer boundary to get entropy at imax
if (cnt==imax)
    Tguess = (T200/boltz)*p(imax)/rho_g(imax); % initial guess to solve Newton-Raphson in Kelvin
    err = 1.e5;
    while (abs(err)>1.e-5)        
        Tguess_keV = Tguess/keV2K;delT_out = Tguess_keV/100;
        alpha_out = 2 - log( lambda_cool(Tguess_keV+delT_out)/lambda_cool(Tguess_keV) )/log( (Tguess_keV+delT_out)/Tguess_keV );
        fn = Tguess^2/lambda_cool(Tguess_keV)/alpha_out - (2/5)*tTIbytf_th*tff(imax)/(boltz^2*mp*mue*mui/mu/fb/d200/T200/p(imax));
        fn_p = Tguess/lambda_cool(Tguess_keV);
        err = -fn/fn_p;
        Tguess = Tguess + err;
        err = err/Tguess;
    end
    Tguess = Tguess*boltz/T200;
    dest(imax) = p(imax)/Tguess;
    K0 = p(imax)/dest(imax)^(5/3);
end
Kest = max(K0,K1);
%fit a high order polynomial for smoothening entropy
pcoeff=polyfit(r,Kest,15);
Ksmt=polyval(pcoeff,r);
Kest=Ksmt; %polynomial fit
%now calculate the new density profile assuming Kest as the entropy
%profile; the equation is solved by Newton Raphson
for i = imax-1:-1:1
    err=1e5;
    d_guess=dest(i+1);
    while (abs(err)>1e-5)
        fzero = -(g_acc(i+1)+g_acc(i))*(r(i+1)-r(i))*( (dest(i+1)+d_guess)/2 )^(-2/3) ...
            - (5/3)*(Kest(i+1)+Kest(i))*(dest(i+1)-d_guess)/(dest(i+1)+d_guess) ...
            -(Kest(i+1)-Kest(i));
        fprime = (1/3)*(g_acc(i+1)+g_acc(i))*(r(i+1)-r(i))*( (dest(i+1)+d_guess)/2 )^(-5/3) ...
            +(10/3)*(Kest(i+1)+Kest(i))*dest(i+1)/(dest(i+1)+d_guess)^2;
        err = - fzero/fprime;
        d_guess = d_guess + err;
        err = err/d_guess;
    end
    dest(i) = d_guess;
end
pest=Kest.*dest.^(5/3);
%now move on to calculate halo-integrated quantities
menc_g_unmod(1) = (4*pi/3)*rho_g(1)*r(1)^3;
menc_g_mod(1) = (4*pi/3)*dest(1)*r(1)^3;
for i = 2:imax %modified and unmodified enclosed gas mass
    menc_g_unmod(i) = menc_g_unmod(i-1) + 4*pi*((0.5*(r(i)+r(i-1)))^2)*(r(i)-r(i-1))*0.5*(rho_g(i-1)+rho_g(i));
    menc_g_mod(i) = menc_g_mod(i-1) + 4*pi*((0.5*(r(i)+r(i-1)))^2)*(r(i)-r(i-1))*0.5*(dest(i-1)+dest(i));
end;
TkeV = T200*(pest./dest)/(boltz*keV2K);
delT = TkeV/100;
dlnLdlnT = log( lambda_cool(TkeV+delT)./lambda_cool(TkeV) )./log( (TkeV+delT)./TkeV ); alpha = (2 - dlnLdlnT);
tcool_est = 1.5*(mue*mui/mu)*(mp./(dest*fb*d200)).*(T200*pest./dest)./lambda_cool(TkeV);
tTI_est = (5/3)*tcool_est./alpha;
ne_est = dest*fb*d200/mue/mp;
%integrated X-ray luminosity
L_X=0;Tlum=0;
for i=1:imax-1
    TkeV0 = T200*( (pest(i+1)+pest(i))/(dest(i)+dest(i+1)) )/(1.3807e-16*1.1604e7);
    n_e = 0.5*(dest(i)+dest(i+1))*fb*d200/(mue*mp); n_i = 0.5*(dest(i)+dest(i+1))*fb*d200/(mui*mp);
    L_X=L_X + 4*pi*r200^3*(r(i+1)-r(i))*(0.5*(r(i)+r(i+1)))^2*lambda_cool(TkeV0)*n_e*n_i;
    Tlum = Tlum + TkeV0*4*pi*r200^3*(r(i+1)-r(i))*(0.5*(r(i)+r(i+1)))^2*lambda_cool(TkeV0)*n_e*n_i;
end
for i = 1: imax-1
fprintf(fid, '%20.7e', 0.5*(r(i)+r(i+1)), ne_est(i), TkeV(i));
fprintf(fid,'\n');
end
%Lx-Tx only for the `core'
L_X_c=0;Tlum_c=0;
for i = 1:cnt-1
    TkeV0 = T200*( (pest(i+1)+pest(i))/(dest(i)+dest(i+1)) )/(1.3807e-16*1.1604e7);
    n_e = 0.5*(dest(i)+dest(i+1))*fb*d200/(mue*mp); n_i = 0.5*(dest(i)+dest(i+1))*fb*d200/(mui*mp);
    L_X_c=L_X_c + 4*pi*r200^3*(r(i+1)-r(i))*(0.5*(r(i)+r(i+1)))^2*lambda_cool(TkeV0)*n_e*n_i;
    Tlum_c = Tlum_c + TkeV0*4*pi*r200^3*(r(i+1)-r(i))*(0.5*(r(i)+r(i+1)))^2*lambda_cool(TkeV0)*n_e*n_i;
end
%Lx-Tx within r500
L_X_500=0;Tlum_500=0;i=1;
while ( 0.5*(r(i)+r(i+1)) <= r500/r200)
    TkeV0 = T200*( (pest(i+1)+pest(i))/(dest(i)+dest(i+1)) )/(1.3807e-16*1.1604e7);
    n_e = 0.5*(dest(i)+dest(i+1))*fb*d200/(mue*mp); n_i = 0.5*(dest(i)+dest(i+1))*fb*d200/(mui*mp);
    L_X_500=L_X_500 + 4*pi*r200^3*(r(i+1)-r(i))*(0.5*(r(i)+r(i+1)))^2*lambda_cool(TkeV0)*n_e*n_i;
    Tlum_500 = Tlum_500 + TkeV0*4*pi*r200^3*(r(i+1)-r(i))*(0.5*(r(i)+r(i+1)))^2*lambda_cool(TkeV0)*n_e*n_i;
    i=i+1;
end
fd = (menc_g_unmod(imax) - menc_g_mod(imax))/menc_g_unmod(imax); Mdot_d = fb*fd*M200/5.e9/2.e33;
eff = L_X*pi*1.e7/Mdot_d/2.e33/3.e10^2;
fd_500 = (menc_g_unmod(i) - menc_g_mod(i))/menc_g_unmod(i);
fprintf(fid1, '%20.7e', M200/2.e33, conc, ne_est(1), TkeV(1)/ne_est(1)^(2/3), ...
    Tlum/L_X, L_X/1.e44, fd, Mdot_d, eff, fb, Tlum_c/L_X_c, L_X_c, c500, M500,...
    r500, Tlum_500/L_X_500, L_X_500, fd_500);
fprintf(fid1,'\n'); %write halo-integrated quantities

end %k
