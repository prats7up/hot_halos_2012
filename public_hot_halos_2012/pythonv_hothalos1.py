#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 10:10:41 2019

@author: prateek
python rewrite of the matlab code. essentially has options for all the 
combinations of halo masses and outer slopes in the same code
add diagnostics for DM, EM, y_{SZ}

de-dimensionalization based on Voit et al. 2002
"""
import math as m
import numpy as np
#import matplotlib.pyplot as plt

def c200_to_cdel(c200,delta): #function that returns cdelta, given a c200
    cdel = c200; err = 1.e10
    rhs =  (200./delta)*c200*c200*c200/( m.log(1+c200)-c200/(1+c200) )
    while (err>1.e-6):
        f = cdel*cdel*cdel/( m.log(1+cdel)-cdel/(1+cdel) ) - rhs
        fprime = 3.*cdel**2/(m.log(1+cdel)-cdel/(1+cdel)) 
        - cdel**4/(1+cdel)**2/(m.log(1+cdel)-cdel/(1+cdel))**2
        err = abs(f/fprime)
        cdel = cdel - f/fprime
    return cdel

def lambda_cool(TkeV): #Tozzi & Norman cooling function for 0.3 solar metallicity
    return 1.e-22*( 8.6e-3*TkeV**-1.7 + 5.8e-2*TkeV**0.5 + 6.3e-2)

def g_BCG(rkpc):
    temp = ( (rkpc**0.5975/3.206e-7)**0.9 + (rkpc**1.849/1.861e-6)**0.9 )
    return 0.*temp**(-1./0.9)

def get_M_c_abyb(os,Mlabel): #returns key halo parameters
#following are the values of abyb for various halos, outer slopes
#OS    halo mass (c200): 3e15 (4), 1e15 (5), 6e14 (6), 3e14 (7), 1e14 (8), 6e13 (9), 3e13 (9.5), 1e13 (10.5), 6e12 (11), 3e12 (11.5), 1e12 (12)
#-1.75                   0.252949, 0.215277, 0.187637, 0.166444, 0.149651, 0.136,    0.130086,   0.119703,    0.115119,  0.110879,    0.106945
#-2.25                   0.449714, 0.41064,  0.380177, 0.355553, 0.335104, 0.317764, 0.310027,   0.296073,    0.289751,  0.283807,    0.278205
#-3                      all 1s here
    tab = np.zeros((4,11))
    tab[0,:] = [3.e15, 1.e15, 6.e14, 3.e14, 1.e14, 6.e13, 3.e13, 1.e13, 6.e12, 
       3.e12, 1.e12]
    tab[1,:] = [4, 5, 6, 7, 8, 9, 9.5, 10.5, 11, 11.5, 12]
    tab[2,:] = [0.252949, 0.215277, 0.187637, 0.166444, 0.149651, 0.136, 
       0.130086, 0.119703, 0.115119, 0.110879, 0.106945]
    tab[3,:] = [0.449714, 0.41064,  0.380177, 0.355553, 0.335104, 0.317764, 
       0.310027, 0.296073, 0.289751, 0.283807, 0.278205]
    if (os==3):
        return tab[0,Mlabel], tab[1,Mlabel], 1.0
    if (os==2.25):
        return tab[0,Mlabel], tab[1,Mlabel], tab[3,Mlabel]
    if (os==1.75):
        return tab[0,Mlabel], tab[1,Mlabel], tab[2,Mlabel]

#constants
imax=1000; zred=0; boltz=1.38066e-16; mu=0.62; mue=1.18; mui=1/(1/mu-1/mue); 
mp=1.673e-24; keV2K=1.1604e7; dcrit = 9.21e-30*(1+zred)**3; d200 = 200*dcrit;
guniv = 6.67e-8; msol=2.e33; clight=3.e10; sig_T=6.65e-25; me=mp/1836.

r = np.logspace(-3,0,imax); dr = np.diff(r); dr = np.insert(dr,0,dr[0]); R = r-0.5*dr #log 1-D grid; r-grid for sphere & R-grid for projection
tTIbytf_th=10 #imposed threshold for min(tTI/tff)

#halo properties
Mlabel=10; outer_slope=2.25 #most massive halo and then label increases with decresing mass
TIcore=0 #constant entropy or tTI/tff core; 0 for constant entropy core
M200, conc, abyb = get_M_c_abyb(outer_slope,Mlabel); M200=M200*msol
c500=c200_to_cdel(conc,500); fb=0.17
r200=( M200/(200*dcrit*(4*m.pi/3)) )**(1./3.);T200=guniv*M200*mu*mp/(2*r200)
r500 = r200*c500/conc; M500 = M200*( m.log(1.+c500) - c500/(1.+c500) )

#NFW profile
rho_dm = ( conc**3/(3*(m.log(1+conc) - conc/(1+conc))) )/(conc*r* (1 + conc*r)**2 )
menc_dm = (np.log(1+ conc*r) - conc*r/(1+conc*r) )/( np.log (1+conc) - conc/(1+conc) )

#BCG gravity from Mathews et al. 2006
rkpc = r*r200/3.086e21; g_ast_cgs = g_BCG(rkpc)
g_ast_nondim = g_ast_cgs/(guniv*M200/r200**2)
g_acc = menc_dm/r**2 + g_ast_nondim

#gas profile not identical to DM, unlike Voit
rho_g = abyb*( conc**3/(3*(m.log(1+conc) - conc/(1+conc))) )/(conc*r* (1 + conc*r)**(outer_slope-1.) )
#outer pressure set such that asymptotic entropy profile is close to r^1.1
p=np.zeros(imax)
p[imax-1]=0.32 #p[imax]=0.25; p[imax] = 0.09;
#obtain unmodified profile assuming a given gas density profile
for i in range(imax-2,-1,-1):
    p[i] = p[i+1] + dr[i]*(g_acc[i]+g_acc[i+1])*0.5*(rho_g[i]+rho_g[i+1])
K1=p/rho_g**(5./3.);
#now convert to dimensional variables to get tTI/tff
tff = m.sqrt(2*r200**3/guniv/M200)*np.sqrt(r**3/menc_dm)
TkeV = T200*(p/rho_g)/(boltz*keV2K); delT = TkeV/100
tcool = 1.5*(mue*mui/mu)*(mp/(rho_g*fb*d200))*(T200*p/rho_g)/lambda_cool(TkeV)
dlnLdlnT = np.log( lambda_cool(TkeV+delT)/lambda_cool(TkeV) )/np.log( (TkeV+delT)/TkeV )
alpha = (2 - dlnLdlnT)
tTI = (5./3.)*tcool/alpha
#dimensionless temperature; guess density
temp = p/rho_g; dest = 1.0*rho_g; cnt=-1000

for i in range(imax-1,-1,-1): #determine where tTI/tff<10
    if ( tTI[i]<tTIbytf_th*tff[i] ):
        cnt=i
        break
K0=K1[0]; #for cases when the core entropy is high already
if (cnt != -1000):
    K0 = K1[cnt];
# if tTI/tff>10 at the outer boundary, solve for the pout=p[imax] and tTI/tff=10 at the outer boundary to get entropy there
if (cnt==imax-1):
    Tguess = (T200/boltz)*p[imax-1]/rho_g[imax-1] #initial guess to solve Newton-Raphson in Kelvin
    err = 1.e5;
    while (abs(err)>1.e-5):
        Tguess_keV = Tguess/keV2K; delT_out = Tguess_keV/100.
        alpha_out = 2 - m.log( lambda_cool(Tguess_keV+delT_out)/lambda_cool(Tguess_keV) )/m.log( (Tguess_keV+delT_out)/Tguess_keV ) #dlnLambda/dlnT
        fn = Tguess**2/lambda_cool(Tguess_keV)/alpha_out - (2./5.)*tTIbytf_th*tff[imax-1]/(boltz**2*mp*mue*mui/mu/fb/d200/T200/p[imax-1])
        fn_p = 2.*Tguess/lambda_cool(Tguess_keV) #not quite correct, but slope is not as important as f itself, so ok
        err = -fn/fn_p; Tguess = Tguess + err; err = err/Tguess
    Tguess = Tguess*boltz/T200; dest[imax-1] = p[imax-1]/Tguess; K0 = p[imax-1]/dest[imax-1]**(5./3.); temp[imax-1]=p[imax-1]/dest[imax-1]

#now calculate modified core
if (TIcore==1): #tTI/tff=10 core
    for i in range(cnt-1,-1,-1):
        fac_g = 0.5*m.log(g_acc[i+1]*r[i]/g_acc[i]/r[i+1])
        TkeV0 = temp[i+1]*T200/(boltz*keV2K); delT0=TkeV0/100
        alpha0 = 2. - m.log( lambda_cool(TkeV0+delT0)/lambda_cool(TkeV0) )/m.log( (TkeV0+delT0)/TkeV0 )
        temp[i] = ( temp[i+1]*(alpha0+0.5*fac_g) + (g_acc[i+1]+g_acc[i])*dr[i] )/(alpha0-0.5*fac_g) #don't quite understand it!
        dest[i] = (5./2./alpha0)*(mue*mui/mu)*mp*temp[i]*T200/(fb*d200)/tTIbytf_th/tff[i]/lambda_cool(TkeV0)
        pest=dest*temp; Kest=pest/dest**(5./3.)

else: #entropy core
    Kest = np.maximum(K0,K1);
    #fit a high order polynomial for smoothening entropy; chose degree 15
    pcoeff = np.polyfit(r,Kest,15); Ksmt = np.polyval(pcoeff,r);
    #now calculate new density profile assuming Kest as entropy profile; solve Newton-Raphson
    for i in range(imax-2,-1,-1):
        err = 1.e5; d_guess = dest[i+1]
        while (abs(err)>1e-5):
            fzero = -(g_acc[i+1]+g_acc[i])*dr[i]*( (dest[i+1]+d_guess)/2. )**(-2./3.) 
            fzero = fzero - (5./3.)*(Kest[i+1]+Kest[i])*(dest[i+1]-d_guess)/(dest[i+1]+d_guess)
            fzero = fzero - (Kest[i+1]-Kest[i])
            fprime = (1./3.)*(g_acc[i+1]+g_acc[i])*dr[i]*( (dest[i+1]+d_guess)/2. )**(-5./3.) 
            fprime = fprime + (10./3.)*(Kest[i+1]+Kest[i])*dest[i+1]/(dest[i+1]+d_guess)**2
            err = -fzero/fprime; d_guess = d_guess + err; err=err/d_guess
        dest[i] = d_guess
    pest=Kest*dest**(5./3.)
n_e = dest*fb*d200/(mue*mp); n_i = n_e*mue/mui; TkeV = T200*pest/dest/(boltz*keV2K)

#DM, y_{tSZ}, EM/SB profiles; projected along LOS; moving from out to in; y_kSZ \propto DM
DM=np.zeros(imax+1); EM=np.zeros(imax+1); y_tSZ=np.zeros(imax+1)
for i in range(imax-1, -1, -1):
    DM[i] = DM[i+1] + 2.*(r200/3.086e18)*n_e[i]*r[i]*dr[i]/m.sqrt(r[i]*r[i]-R[i]*R[i]) #pc cm^{-3}
    EM[i] = EM[i+1] + 2.*(r200/3.086e18)*n_e[i]*n_e[i]*r[i]*dr[i]/m.sqrt(r[i]*r[i]-R[i]*R[i]) #pc cm^{-6}
    y_tSZ[i] = y_tSZ[i+1] + 2.*(sig_T*r200/(me*clight**2))*n_e[i]*boltz*keV2K*TkeV[i]*r[i]*dr[i]/m.sqrt(r[i]*r[i]-R[i]*R[i]) #dimensionless
DM=DM[:-1]; EM=EM[:-1]; y_tSZ=y_tSZ[:-1] 
#halo integrated quantities
menc_g_unmod=np.zeros(imax); menc_g_mod=np.zeros(imax); menc_g_unmod[0]=(4.*m.pi/3.)*rho_g[0]*R[0]**3; menc_g_mod[0]=(4.*m.pi/3.)*dest[0]*R[0]**3   #first-order accurate estimate   
for i in range(1,imax):
    menc_g_unmod[i] =  menc_g_unmod[i-1] + 4.*m.pi*r[i-1]**2*dr[i]*rho_g[i-1]
    menc_g_mod[i] =  menc_g_mod[i-1] + 4.*m.pi*r[i-1]**2*dr[i]*dest[i-1]
L_X=0.; Tlum=0.; Y_tSZ=0. #volume-integrated X-ray luminosity & temperature & Y_tSZ
for i in range(imax-1):
    L_X=L_X + 4*m.pi*r200**3*dr[i]*r[i]**2*lambda_cool(TkeV[i])*n_e[i]*n_i[i]
    Tlum = Tlum + TkeV[i]*4*m.pi*r200**3*dr[i]*r[i+1]**2*lambda_cool(TkeV[i])*n_e[i]*n_i[i]
    Y_tSZ = Y_tSZ + (sig_T*r200**3/(3.086e24**2*me*clight**2))*n_e[i]*boltz*keV2K*TkeV[i]*4*m.pi*dr[i]*r[i+1]**2 #in Mpc^2
L_X_c=0.; Tlum_c=0.; Y_tSZ_c=0. #only core
for i in range(cnt-1):
    L_X_c=L_X_c + 4*m.pi*r200**3*dr[i]*r[i+1]**2*lambda_cool(TkeV[i])*n_e[i]*n_i[i]
    Tlum_c = Tlum_c + TkeV[i]*4*m.pi*r200**3*dr[i]*R[i+1]**2*lambda_cool(TkeV[i])*n_e[i]*n_i[i]
    Y_tSZ_c = Y_tSZ_c + (sig_T*r200**3/(3.086e24**2*me*clight**2))*n_e[i]*boltz*keV2K*TkeV[i]*4*m.pi*dr[i]*r[i+1]**2 #in Mpc^2
L_X_500=0.; Tlum_500=0.; Y_tSZ_500=0. #within r500
while ( 0.5*(r[i]+r[i+1]) <= r500/r200 ):
    L_X_500=L_X_500 + 4*m.pi*r200**3*dr[i]*R[i+1]**2*lambda_cool(TkeV[i])*n_e[i]*n_i[i]
    Tlum_500 = Tlum_500 + TkeV[i]*4*m.pi*r200**3*dr[i]*R[i+1]**2*lambda_cool(TkeV[i])*n_e[i]*n_i[i]
    Y_tSZ_500 = Y_tSZ_500 + (sig_T*r200**3/(3.086e24**2*me*clight**2))*n_e[i]*boltz*keV2K*TkeV[i]*4*m.pi*dr[i]*r[i+1]**2 #in Mpc^2
    i = i+1
#dropout fraction and other quantities in the table in the paper
fd = (menc_g_unmod[imax-1] - menc_g_mod[imax-1])/menc_g_unmod[imax-1]; Mdot_d = fb*fd*M200/5.e9/msol
eff = L_X*m.pi*1.e7/Mdot_d/msol/clight**2; fd_500 = (menc_g_unmod[i-1] - menc_g_mod[i-1])/menc_g_unmod[i-1]
Tlum=Tlum/(L_X+1.e-20); Tlum_c=Tlum_c/(L_X_c+1.e-20); Tlum_500=Tlum_500/(L_X_500+1.e-20)